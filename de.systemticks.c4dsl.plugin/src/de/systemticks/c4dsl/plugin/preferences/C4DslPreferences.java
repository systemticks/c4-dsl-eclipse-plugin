package de.systemticks.c4dsl.plugin.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

public class C4DslPreferences {

	public final static String PREF_INLINE_RENDERER = "c4.diagram.renderer";
	
	public final static String PREF_KROKI_ENABLED = "c4.diagram.plantuml.enabled";
	
	public final static String PREF_KROKI_URL = "https://kroki.io";

    public final static ScopedPreferenceStore scopedPreferenceStore = new ScopedPreferenceStore(InstanceScope.INSTANCE,
            "de.systemticks.c4dsl.plugin");

}
