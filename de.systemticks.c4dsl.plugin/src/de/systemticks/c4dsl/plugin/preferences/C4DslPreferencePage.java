package de.systemticks.c4dsl.plugin.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import static de.systemticks.c4dsl.plugin.preferences.C4DslPreferences.*;

public class C4DslPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public C4DslPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(scopedPreferenceStore);
		setDescription("A demonstration of a preference page implementation");

	}

	@Override
	protected void createFieldEditors() {
		addField(new RadioGroupFieldEditor(PREF_INLINE_RENDERER, "Inline Renderer", 1, new String[][] { { "Plant UML", "plantuml" },
				{ "Mermaid", "mermaid" }, { "Structurizr", "structurizr" } }, getFieldEditorParent()));

		addField(new BooleanFieldEditor(PREF_KROKI_ENABLED,
				"Use https://kroki.io/ for rendering purposes. Do not enable, if you have concerns.",
				getFieldEditorParent()));
		
		addField(
				new StringFieldEditor(PREF_KROKI_URL, "Kroki Server URL", getFieldEditorParent()));

	}

}
