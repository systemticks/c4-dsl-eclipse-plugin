package de.systemticks.c4dsl.plugin.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import static de.systemticks.c4dsl.plugin.preferences.C4DslPreferences.*;

public class C4DslPreferencesInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {

		scopedPreferenceStore.setDefault(PREF_KROKI_URL, "https://kroki.io");

		scopedPreferenceStore.setDefault(PREF_KROKI_ENABLED, false);
		
		scopedPreferenceStore.setDefault(PREF_INLINE_RENDERER, "plantuml");
	}

}
