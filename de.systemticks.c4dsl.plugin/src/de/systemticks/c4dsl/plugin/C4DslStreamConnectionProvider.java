package de.systemticks.c4dsl.plugin;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.lsp4e.server.ProcessStreamConnectionProvider;
import org.osgi.framework.Bundle;
import static de.systemticks.c4dsl.plugin.preferences.C4DslPreferences.*;

public class C4DslStreamConnectionProvider extends ProcessStreamConnectionProvider {

	public C4DslStreamConnectionProvider() {

        String batchLocation = "server/c4-language-server/bin"; 
        Bundle bundle = Platform.getBundle("de.systemticks.c4dsl.plugin");
        
		URL url = FileLocator.find(bundle, new Path(batchLocation), null);

		try {
			url = FileLocator.toFileURL(url);
			File file = URIUtil.toFile(URIUtil.toURI(url));
	        List<String> commands = new ArrayList<>();
	        
	        System.out.println("***** Start the language server from here: "+file.getAbsolutePath());
	        
	        commands.add(file.getAbsolutePath() + "/c4-language-server.bat");
	        commands.add("-ir="+retrieveInlineRenderer());
	        setCommands(commands);
			setWorkingDirectory(System.getProperty("user.dir"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		       
	}
	
	private String retrieveInlineRenderer() {
		return scopedPreferenceStore.getString(PREF_INLINE_RENDERER);
	}
	
}
