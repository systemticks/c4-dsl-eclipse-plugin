package de.systemticks.c4dsl.plugin.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DiagramProvider {

	private final static String RENDER_PLANTUML_AS_SVG_URL = "https://kroki.io/plantuml/svg/";	
	private final static String RENDER_MERMAID_AS_SVG_URL = "https://mermaid.ink/svg/";
	
	public String getSVGFromPlantUmlCode(String encodedPlantUml) throws IOException {
		return doRequest(new URL(RENDER_PLANTUML_AS_SVG_URL + encodedPlantUml));
	}

	public String getSVGFromMermaidCode(String encodedPlantUml) throws IOException {
		return doRequest(new URL(RENDER_MERMAID_AS_SVG_URL + encodedPlantUml));
	}

	private String doRequest(URL url) throws IOException {
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		
		return content.toString();
	}

	
}
