package de.systemticks.c4dsl.plugin.handler;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.lsp4e.command.LSPCommandHandler;
import org.eclipse.lsp4j.Command;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import de.systemticks.c4dsl.plugin.previews.UmlPreview;
import de.systemticks.c4dsl.plugin.provider.DiagramProvider;

@SuppressWarnings("restriction")
public class C4ShowMermaidHandler extends LSPCommandHandler {
	
	private final DiagramProvider mermaidDiagramProvider;

	public C4ShowMermaidHandler() {
		mermaidDiagramProvider = new DiagramProvider();
	}
	
	@Override
	public Object execute(ExecutionEvent event, Command command, IPath arg2) throws ExecutionException {
		
		try {
			String svg = mermaidDiagramProvider.getSVGFromMermaidCode((String) command.getArguments().get(0));
			openInBrowser(event, svg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	private void openInBrowser(ExecutionEvent event, String html) {
		try {
			UmlPreview preview = (UmlPreview) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView("de.systemticks.c4dsl.plugin.plantuml");
			preview.setContent(html);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
