package de.systemticks.c4dsl.plugin.previews;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public class UmlPreview extends ViewPart {

	private Browser browser;

	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
		browser = new Browser(parent, SWT.WEBKIT);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
	
	public void setContent(Object content) {
	   browser.setText(createHTML((String) content));
	}
	
	private String createHTML(String svg) {
		
		StringBuffer buffer = new StringBuffer();
		buffer
			.append("<!DOCTYPE html>")
			.append("<html lang=\"en\">")
			.append("<head>")
			.append("<head>")
			.append("<meta charset=\"UTF-8\">")
			.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">")
			.append("<title>PlantUML Preview</title>")
			.append("</head>")
			.append("<body>")
			.append(svg)
			.append("</body>")
			.append("</html>");
		
		return buffer.toString();
	}
	
}
